//
//  ChatVC.m
//  KwikVet
//
//  Created by Elluminati Macbook Pro 2 on 11/22/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import "ChatVC.h"
#import "MessageTableViewCell.h"

@interface ChatVC ()
{
    NSMutableDictionary *dictWalker;
    NSMutableArray *arrForChat,*arrForDate,*arrForSection;
    float tableHeight,viewPosition;
    CGRect frameForTable;
    BOOL isFirstLoad,isStartNewChat;
}

@end

@implementation ChatVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"walker = %@",self.dictHistoryInfo);
    [self customSetup];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getChatHistory];
    [self.timerForChat invalidate];
    self.timerForChat = nil;
    self.timerForChat = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(getChatHistory) userInfo:nil repeats:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.timerForChat invalidate];
    self.timerForChat = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Custom Methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    // Disallow recognition of tap gestures in the segmented control.
    if ((touch.view == self.tableForChat))
    {
        NSLog(@"Touch on tableView");
        return NO;
    }
    return YES;
}

-(void)customSetup
{
    self.tableForChat.allowsSelection = NO;
    isFirstLoad=NO;
    isStartNewChat=NO;
    
    viewPosition = self.viewForKeyboard.frame.origin.y;
    tableHeight = self.tableForChat.frame.size.height;
    frameForTable = self.tableForChat.frame;
    
    arrForChat = [[NSMutableArray alloc] init];
    dictWalker = [[NSMutableDictionary alloc] initWithDictionary:[self.dictHistoryInfo valueForKey:@"walker"]];
    self.lblProviderName.text = [NSString stringWithFormat:@"%@ %@",[dictWalker valueForKey:@"first_name"],[dictWalker valueForKey:@"last_name"]];
    
    [self.btnChat setTitle:NSLocalizedString(@"CHAT", nil) forState:UIControlStateNormal];
    [self.btnSend setTitle:NSLocalizedString(@"SEND", nil) forState:UIControlStateNormal];
    self.btnSend.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getTouch:)];
    [self.view addGestureRecognizer:letterTapRecognizer];
    
    self.txtMessage.placeholder = NSLocalizedString(@"ENTER_MESSAGE", nil);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHidden:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)getTouch:(UITapGestureRecognizer*)sender
{
    UIView *view = sender.view;
    NSLog(@"%ld", (long)view.tag);
    
    if(view.tag==0)
    {
        [self.view endEditing:YES];
    }
}

-(void)getChatHistory
{
    if([APPDELEGATE connected])
    {
        if(isFirstLoad==NO)
        {
            isFirstLoad=YES;
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"GETTING_CHATS", nil)];
        }
        
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc] init];
        
        [dictParam setValue:[NSString stringWithFormat:@"%@",[self.dictHistoryInfo valueForKey:@"id"]] forKey:PARAM_REQUEST_ID];
        [dictParam setValue:[PREF objectForKey:PREF_USER_ID] forKey:PARAM_SENDER_ID];
        [dictParam setValue:[NSString stringWithFormat:@"%@",[dictWalker valueForKey:@"walker_id"]] forKey:PARAM_RECEIVER_ID];
        [dictParam setValue:[PREF objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setValue:@"0" forKey:PARAM_LAST_ID];
        
        AFNHelper *afn = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_CHAT_HISTORY withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSMutableArray *arr = [[NSMutableArray alloc] init];
                     [arr addObjectsFromArray:[response valueForKey:@"data"]];
                     if(arr.count!=0)
                     {
                         if(arr.count!=arrForChat.count)
                         {
                             [arrForChat removeAllObjects];
                             [arrForChat addObjectsFromArray:arr];
                             [self makeSection];
                             //self.tableForChat.hidden=NO;
                             self.imgNoChats.hidden=YES;
                             self.lblProviderName.hidden=NO;
                             
                             [UIView animateWithDuration:0 delay:0.1 options:0 animations:^{
                                 
                                 [self.tableForChat scrollRectToVisible:CGRectMake(0, self.tableForChat.contentSize.height - self.tableForChat.bounds.size.height, self.tableForChat.bounds.size.width, self.tableForChat.bounds.size.height+20) animated:NO];
                                 
                             } completion:^(BOOL finished) {
                                 self.tableForChat.hidden=NO;
                             }];// for delay to realod and scroll down tableview to bottom
                         }
                     }
                     else
                     {
                         self.tableForChat.hidden=YES;
                         
                         self.lblProviderName.hidden=YES;
                         
                         if(isStartNewChat==YES)
                             self.imgNoChats.hidden=YES;
                         else
                             self.imgNoChats.hidden=NO;
                     }
                 }
                 else
                 {
                     
                 }
             }
             [APPDELEGATE hideLoadingView];
         }];
    }
}

-(void)makeSection
{
    NSMutableArray *arrTemp=[[NSMutableArray alloc] init];
    [arrTemp addObjectsFromArray:arrForChat];
    for (int s=0; s<[arrTemp count]; s++)
    {
        NSMutableDictionary *dictTrip = [[arrTemp objectAtIndex:s] mutableCopy];
        dictTrip = [super cleanNullInJsonDic:dictTrip];
        [arrTemp replaceObjectAtIndex:s withObject:dictTrip];
    }
    arrForDate=[[NSMutableArray alloc]init];
    arrForSection=[[NSMutableArray alloc]init];
    NSMutableArray *arrtemp=[[NSMutableArray alloc]init];
    [arrtemp addObjectsFromArray:arrTemp];
    NSSortDescriptor *distanceSortDiscriptor = [NSSortDescriptor sortDescriptorWithKey:@"msg_date" ascending:YES selector:@selector(localizedStandardCompare:)];
    [arrtemp sortUsingDescriptors:@[distanceSortDiscriptor]];
    
    for (int i=0; i<arrtemp.count; i++)
    {
        NSMutableDictionary *dictDate=[[NSMutableDictionary alloc]init];
        dictDate=[arrtemp objectAtIndex:i];
        
        NSString *temp=[dictDate valueForKey:@"msg_date"];
        NSArray *arrDate=[temp componentsSeparatedByString:@" "];
        NSString *strdate=[arrDate objectAtIndex:0];
        if(![arrForDate containsObject:strdate])
        {
            [arrForDate addObject:strdate];
        }
    }
    for (int j=0; j<arrForDate.count; j++)
    {
        NSMutableArray *a=[[NSMutableArray alloc]init];
        [arrForSection addObject:a];
    }
    for (int j=0; j<arrForDate.count; j++)
    {
        NSString *strTempDate=[arrForDate objectAtIndex:j];
        
        for (int i=0; i<arrtemp.count; i++)
        {
            NSMutableDictionary *dictSection=[[NSMutableDictionary alloc]init];
            dictSection=[arrtemp objectAtIndex:i];
            NSArray *arrDate=[[dictSection valueForKey:@"msg_date"] componentsSeparatedByString:@" "];
            NSString *strdate=[arrDate objectAtIndex:0];
            if ([strdate isEqualToString:strTempDate])
            {
                [[arrForSection objectAtIndex:j] addObject:dictSection];
            }
        }
    }
    
    [self.tableForChat reloadData];
}

-(void)sendMessage:(NSString*)message
{
    if ([APPDELEGATE connected])
    {
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        
        [dictParam setValue:[NSString stringWithFormat:@"%@",[self.dictHistoryInfo valueForKey:@"id"]] forKey:PARAM_REQUEST_ID];
        [dictParam setValue:[PREF objectForKey:PREF_USER_ID] forKey:PARAM_SENDER_ID];
        [dictParam setValue:[NSString stringWithFormat:@"%@",[dictWalker valueForKey:@"walker_id"]] forKey:PARAM_RECEIVER_ID];
        [dictParam setValue:[PREF objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setValue:@"0" forKey:PARAM_LAST_ID];
        [dictParam setValue:message forKey:PARAM_MESSAGE];
        [dictParam setValue:@"0" forKey:PARAM_RECEIVER_TYPE];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [helper getDataFromPath:FILE_SEND_MESSAGE withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"] boolValue])
                 {
                     //[arrMessages removeAllObjects];
                     //[arrMessages addObjectsFromArray:[response valueForKey:@"message"]];
                     //[self makeSection];
                     [self getChatHistory];
                 }
                 else
                 {
                     [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                 }
             }
         }];
    }
}

#pragma mark -
#pragma mark - Keyboard Methods

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [self.viewForKeyboard setFrame:CGRectMake(self.viewForKeyboard.frame.origin.x,self.view.frame.size.height - keyboardSize.height - self.viewForKeyboard.frame.size.height, self.viewForKeyboard.frame.size.width, self.viewForKeyboard.frame.size.height)];
    
    self.tableForChat.frame = CGRectMake(frameForTable.origin.x, frameForTable.origin.y, frameForTable.size.width, frameForTable.size.height-keyboardSize.height);
    
    [self.tableForChat scrollRectToVisible:CGRectMake(0, self.tableForChat.contentSize.height - self.tableForChat.bounds.size.height, self.tableForChat.bounds.size.width, self.tableForChat.bounds.size.height+20) animated:NO];
}

- (void)keyboardWasHidden:(NSNotification *)notification
{
    [self.viewForKeyboard setFrame:CGRectMake(self.viewForKeyboard.frame.origin.x, viewPosition, self.viewForKeyboard.frame.size.width, self.viewForKeyboard.frame.size.height)];
}

#pragma mark -
#pragma mark - Action Methods

- (IBAction)onClickBack:(id)sender
{
    [self.timerForChat invalidate];
    self.timerForChat = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickSend:(id)sender
{
    NSData *data = [self.txtMessage.text dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    if ([text length] != 0)
    {
        [self sendMessage:text];
        [self.txtMessage setText:@""];
    }
}

#pragma mark -
#pragma mark - table view Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrForSection.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [[arrForSection objectAtIndex:section] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    headerView.backgroundColor=[[UIColor alloc] initWithRed:239.0/255.0f green:239.0/255.0f blue:244.0/255.0f alpha:1];
    
    UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 20)];
    lblDate.font=[UberStyleGuide fontRegular:13.0f];
    lblDate.textAlignment=NSTextAlignmentCenter;
    lblDate.textColor=[UIColor darkGrayColor];
    NSString *strDate=[arrForDate objectAtIndex:section];
    NSString *current=[[UtilityClass sharedObject] DateToString:[NSDate date] withFormate:@"yyyy-MM-dd"];
    
    ///   YesterDay Date Calulation
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = -1;
    NSDate *yesterday = [gregorian dateByAddingComponents:dayComponent
                                                   toDate:[NSDate date]
                                                  options:0];
    NSString *strYesterday=[[UtilityClass sharedObject] DateToString:yesterday withFormate:@"yyyy-MM-dd"];
    
    
    if([strDate isEqualToString:current])
    {
        lblDate.text=@"Today";
    }
    else if ([strDate isEqualToString:strYesterday])
    {
        lblDate.text=@"Yesterday";
    }
    else
    {
        NSDate *date=[[UtilityClass sharedObject]stringToDate:strDate withFormate:@"yyyy-MM-dd"];
        NSString *text=[[UtilityClass sharedObject]DateToString:date withFormate:@"dd MMMM yyyy"];//2nd Jan 2015
        lblDate.text=text;
    }
    
    [headerView addSubview:lblDate];
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *entry=[[arrForSection objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    
    NSLog(@"Data entry = %@",entry);
    
    MessageTableViewCell *cell;
    
    if([[entry valueForKey:@"sender"] isEqual:[PREF objectForKey:PREF_USER_ID]])
        //if(indexPath.row %2 ==0)
    {
        cell = [self.tableForChat dequeueReusableCellWithIdentifier:@"OutgoingMessageCell"];
        
        if (!cell)
        {
            cell = [[NSBundle mainBundle] loadNibNamed:@"OutgoingMessageCell" owner:self options:nil][0];
            cell.backgroundColor = [UIColor clearColor];
            cell.contentView.backgroundColor = [UIColor clearColor];
        }
        
        [cell.bubbleView setBackgroundColor:[UIColor colorWithRed:204/255.0f green:233/255.0f blue:182/255.0f alpha:1.0]];
        [cell.lblMessage setTextAlignment:NSTextAlignmentRight];
        [cell.lblMessage setTextColor:[UIColor blackColor]];
    }
    else
    {
        cell = [self.tableForChat dequeueReusableCellWithIdentifier:@"IncomingMessageCell"];
        
        if (!cell)
        {
            cell = [[NSBundle mainBundle] loadNibNamed:@"IncomingMessageCell" owner:self options:nil][0];
            cell.backgroundColor = [UIColor clearColor];
            cell.contentView.backgroundColor = [UIColor clearColor];
        }
        
        [cell.bubbleView setBackgroundColor:[UIColor colorWithRed:209/255.0f green:209/255.0f blue:209/255.0f alpha:1.0]];
        [cell.lblMessage setTextAlignment:NSTextAlignmentLeft];
        [cell.lblMessage setTextColor:[UIColor blackColor]];
    }
    
    [cell setCellData:entry withParent:self];
    [cell.bubbleView.layer setCornerRadius:12.0];
    
    self.tableForChat.rowHeight = cell.bubbleView.frame.size.height+10;
    
    cell.bubbleImage.frame = CGRectMake(cell.bubbleImage.frame.origin.x, self.tableForChat.rowHeight/2-5, cell.bubbleImage.frame.size.width, cell.bubbleImage.frame.size.height);
    
    cell.bubbleImageRight.frame = CGRectMake(cell.bubbleImageRight.frame.origin.x+4,self.tableForChat.rowHeight/2-5, cell.bubbleImageRight.frame.size.width, cell.bubbleImageRight.frame.size.height);
    
    NSLog(@"%f",self.tableForChat.rowHeight);
    
    return cell;
}

#pragma mark -
#pragma mark - textField Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.text.length==0)
        self.txtMessage.text = @"";
    if(arrForChat.count==0)
    {
        isStartNewChat=YES;
        self.imgNoChats.hidden=YES;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if([textField.text isEqualToString:NSLocalizedString(@"ENTER_MESSAGE", nil)])
        self.txtMessage.text = @"";
    
    if(arrForChat.count==0)
    {
        isStartNewChat=NO;
        self.imgNoChats.hidden=NO;
    }
    
    [self.tableForChat setFrame:frameForTable];
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    [textField resignFirstResponder];
}

@end