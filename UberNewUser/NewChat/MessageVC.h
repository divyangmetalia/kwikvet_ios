//
//  MessageVC.h
//  OceanPark
//
//  Created by Elluminati on 03/10/15.
//  Copyright (c) 2015 Elluminati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

@interface MessageVC : BaseVC <BaseDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnCalendar;
@property (weak, nonatomic) IBOutlet UIButton *btnManuel;
@property (weak, nonatomic) IBOutlet UIButton *btnNews;
@property (weak, nonatomic) IBOutlet UIButton *btnMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnNewsletter;
@property (weak, nonatomic) IBOutlet UIView *viewForSearch;
@property (weak, nonatomic) IBOutlet UIView *viewForSegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentForChatContactList;
@property (weak, nonatomic) IBOutlet UITableView *tableViewForContactList;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;

- (IBAction)btnCalendarClicked:(id)sender;
- (IBAction)btnManuelClicked:(id)sender;
- (IBAction)btnNewsClicked:(id)sender;
- (IBAction)btnNewsletterClicked:(id)sender;
- (IBAction)segmentChanged:(UISegmentedControl *)sender;

//chat

- (IBAction)onClickforBack:(id)sender;
- (IBAction)onClickforSend:(id)sender;

@property (strong,nonatomic) NSMutableDictionary *dictData;

@property (weak, nonatomic) IBOutlet UIView *viewForChat;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnSendMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UITableView *tblSendChat;
@property (weak, nonatomic) IBOutlet UITextField *txtMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *noItem;
@property (weak, nonatomic) IBOutlet UILabel *lblNoItem;

@end