//
//  CellSendChat.h
//  TrocaTroca
//
//  Created by Rahul on 16/08/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellSendChat : UITableViewCell
{
    id cellParent;
}

@property (weak, nonatomic) IBOutlet UIView *viewInner;
@property (weak, nonatomic) IBOutlet UILabel *lblChat;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIImageView *imageForChatBackground;

- (void)setCellData:(NSDictionary *)dictData withParent:(id)parent ;
- (float) getHeight;

@end