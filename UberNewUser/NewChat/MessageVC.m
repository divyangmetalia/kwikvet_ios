//
//  MessageVC.m
//  OceanPark
//
//  Created by Elluminati on 03/10/15.
//  Copyright (c) 2015 Elluminati. All rights reserved.
//

#import "MessageVC.h"
#import "ChatContactListCell.h"
#import "ChatRecentContactListCell.h"
#import "UIImageView+Download.h"
#import "CellSendChat.h"
#import "CellReceiveChat.h"

@interface MessageVC ()
{
    NSMutableArray *arrForContactList;
    NSString *segmentString, *strReceiverId, *staffNumber, *strLastMessageTime;
    NSMutableArray *arrdata,*arrChat, *arrLastMessageData;
    NSTimer *timer;
    NSMutableArray *arrForDate;
    NSMutableArray *arrForSection;
}

@end

@implementation MessageVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    //super.delegate = self;
    staffNumber = [[NSUserDefaults standardUserDefaults]objectForKey:@"staffNumber"];
    segmentString = @"Contact List";
    arrForContactList = [[NSMutableArray alloc]init];

    self.tableViewForContactList.delegate = self;
    self.tableViewForContactList.dataSource = self;
    //[super setLeftSide:[UIImage imageNamed:@"Home"] title:nil];
    //[super setRightSide:[UIImage imageNamed:@"Notification"] title:nil];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0f/255.0f green:195.0f/255.0f blue:255.0f/255.0f alpha:1.0];

    //chat
    arrdata=[[NSMutableArray alloc] init];
    arrChat=[[NSMutableArray alloc] init];
    arrLastMessageData=[[NSMutableArray alloc] init];

    UINib *cellSendChat = [UINib nibWithNibName:@"CellSendChat" bundle:nil];
    [self.tblSendChat registerNib:cellSendChat forCellReuseIdentifier:@"CellSendChat"];
    UINib *cellReceiveChat = [UINib nibWithNibName:@"CellReceiveChat" bundle:nil];
    [self.tblSendChat registerNib:cellReceiveChat forCellReuseIdentifier:@"CellReceiveChat"];

    NSLog(@"dict is :%@",self.dictData);
    //timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(timerCalled) userInfo:nil repeats:YES];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didChangePreferredContentSize:)
                                                 name:UIContentSizeCategoryDidChangeNotification object:nil];

    self.lblNoItem.text = NSLocalizedString(@"NO_ITEM", nil);
    CGRect rect = self.tblSendChat.frame;
    rect.size.width = self.view.frame.size.width;
    self.tblSendChat.frame = rect;
    self.viewForChat.hidden = YES;
    self.btnSendMessage.enabled = NO;
    self.btnSendMessage.backgroundColor = [UIColor lightGrayColor];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [super setNavBarTitle:@"Message"];
    self.segmentForChatContactList.layer.cornerRadius = 15.0f;
    self.segmentForChatContactList.layer.borderColor = [UIColor colorWithRed:0.0f/255.0f green:198.0f/255.0f blue:252.0f/255.0f alpha:1.0f].CGColor;
    self.segmentForChatContactList.layer.borderWidth = 1.0f;
    self.segmentForChatContactList.layer.masksToBounds = YES;
    [self getContactList];
}

-(void)getContactList
{
    /*if([APPDELEGATE connected])
    {
        [[AppDelegate sharedAppDelegate]showHUDLoadingView:NSLocalizedString(@"Loading", nil)];
        
        //AFBaseHandler *handler = [[AFBaseHandler alloc]init];
        [handler startHTTPRequestNoPressMessageHandlersURL:FILE_CONTACT_LIST param:nil successBlock:^(id response)
         {
             [[AppDelegate sharedAppDelegate]hideHUDLoadingView];
             NSLog(@"contact list response = %@",response);
             [arrForContactList removeAllObjects];
             [arrForContactList addObjectsFromArray:[response valueForKey:@"json"]];
             if([[response valueForKey:@"msg"] isEqualToString:@"success"])
             {
                 if(arrForContactList.count > 0)
                 {
                     for(int i=0; i<arrForContactList.count; i++)
                     {
                         if([[[arrForContactList objectAtIndex:i]valueForKey:@"staffNumber"] isEqualToString:staffNumber])
                         {
                             [arrForContactList removeObjectAtIndex:i];
                         }
                     }
                     [self.tableViewForContactList reloadData];
                 }
                 else
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"No Data", nil)];
             }
         }
        failerBlock:^(id obj)
         {
             [[AppDelegate sharedAppDelegate]hideHUDLoadingView];
             NSLog(@"error device add url = %@",obj);
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Internet Connection Error", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
        alert.tag = 101;
        [alert show];
    }*/
}

#pragma mark - UIAlertview Method

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 101)
    {
        if (buttonIndex == 1)
        {
            exit(0);
        }
    }
}

#pragma mark-
#pragma mark- On Click Methods

/*-(void)onClickLeftSide
{
    MenuVC *menu = [[MenuVC alloc] initWithNibName:@"MenuVC" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:menu];
    [self presentViewController:navigationController animated:NO completion:nil];
}

- (IBAction)btnCalendarClicked:(id)sender
{
    Calendar3Day *calendar = [[Calendar3Day alloc] initWithNibName:@"Calendar3Day" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:calendar];
    [self presentViewController:navigationController animated:NO completion:nil];
}

- (IBAction)btnManuelClicked:(id)sender
{
    ManuelVC *manuel = [[ManuelVC alloc] initWithNibName:@"ManuelVC" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:manuel];
    [self presentViewController:navigationController animated:NO completion:nil];
}

- (IBAction)btnNewsClicked:(id)sender
{
    NewsVC *news = [[NewsVC alloc] initWithNibName:@"NewsVC" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:news];
    [self presentViewController:navigationController animated:NO completion:nil];
}

- (IBAction)btnNewsletterClicked:(id)sender
{
    NewsletterVC *newsletter = [[NewsletterVC alloc] initWithNibName:@"NewsletterVC" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newsletter];
    [self presentViewController:navigationController animated:NO completion:nil];
}*/

#pragma mark -
#pragma mark - TableView Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == self.tblSendChat)
        return arrForSection.count;
    else
        return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.tblSendChat)
        return [[arrForSection objectAtIndex:section] count];
    else
        return arrForContactList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tblSendChat)
    {
        NSDictionary *dict = [[arrForSection objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];

        if(([[dict valueForKey:@"receiver"] isEqualToString:strReceiverId] || [[dict valueForKey:@"receiver"] isEqualToString:staffNumber]) && ([[dict valueForKey:@"sender"] isEqualToString:strReceiverId] || [[dict valueForKey:@"sender"] isEqualToString:staffNumber]))
        {
            if([[dict valueForKey:@"sender"] isEqualToString:strReceiverId])
            {
                CellSendChat *cell = (CellSendChat *)[tableView dequeueReusableCellWithIdentifier:@"CellSendChat"];
                cell.backgroundColor = [UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1.0f];
                [cell setCellData:dict withParent:self];
                return cell;
            }
            else
            {
                CellReceiveChat *cell = (CellReceiveChat *) [tableView dequeueReusableCellWithIdentifier:@"CellReceiveChat"];
                cell.backgroundColor = [UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1.0f];
                [cell setRecivedChatData:dict strImageForSelller:nil perent:self];
                return cell;
            }
        }
        else
        {
            CellSendChat *cell = (CellSendChat *)[tableView dequeueReusableCellWithIdentifier:@"CellSendChat"];
            [cell setCellData:nil withParent:self];
            return cell;
        }
    }
    else
    {
        if([segmentString isEqualToString:@"Contact List"])
        {
            static NSString *identifier = @"ChatContactListCell";
            ChatContactListCell *cell = (ChatContactListCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"ChatContactListCell" bundle:nil] forCellReuseIdentifier:identifier];
                cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            }
            NSString *key = @"innoways21897016";
            NSString *iv = @"61079821218970166107982121897016";
            //NSData *keyData = [[NSData alloc] initWithBase64EncodedString:key options:0];
            //NSData *ivData = [[NSData alloc] initWithBase64EncodedString:iv options:0];

            NSString *encrypted = [[arrForContactList objectAtIndex:indexPath.row]valueForKey:@"staffName"];
            NSData *data = [encrypted dataUsingEncoding:NSUTF8StringEncoding];

            /*NSData *dataEncrypted = [data AES256EncryptWithKey:key andIV:iv];
            NSString *base64String = [dataEncrypted base64EncodedStringWithOptions:0];
            NSLog(@"base64 encryot string = %@",base64String);*/

            NSData *dataDecrypted = @"";
            NSLog(@"decrypt data = %@",dataDecrypted);
            NSString *receivedDataDecryptString = [[NSString alloc]initWithData:dataDecrypted encoding:NSUTF8StringEncoding];
            NSLog(@"decrypt string = %@",receivedDataDecryptString);

            cell.lblName.text = [[arrForContactList objectAtIndex:indexPath.row]valueForKey:@"staffName"];
            [cell.imageViewForChatContact downloadFromURL:[[arrForContactList objectAtIndex:indexPath.row]valueForKey:@"staffPhoto"] withPlaceholder:[UIImage imageNamed:@"user"]];
            return cell;
        }
        else
        {
            static NSString *identifier = @"ChatRecentContactListCell";
            ChatRecentContactListCell *cell = (ChatRecentContactListCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"ChatRecentContactListCell" bundle:nil] forCellReuseIdentifier:identifier];
                cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            }
            cell.lblName.text = [[arrForContactList objectAtIndex:indexPath.row]valueForKey:@"staffName"];
            [cell.imageViewForChatContact downloadFromURL:[[arrForContactList objectAtIndex:indexPath.row]valueForKey:@"staffPhoto"] withPlaceholder:[UIImage imageNamed:@"user"]];
            cell.lblLastMessage.text = //[self getLastMessage:[[arrForContactList objectAtIndex:indexPath.row]valueForKey:@"staffNumber"]];
            cell.lblLastMessageTime.text = strLastMessageTime;
            return cell;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView != self.tblSendChat)
    {
        self.lblTitle.text = [[arrForContactList objectAtIndex:indexPath.row]valueForKey:@"staffName"];
        strReceiverId = [[arrForContactList objectAtIndex:indexPath.row]valueForKey:@"staffNumber"];
        //[self getAllConverstion];
        self.txtMessage.text = @"";
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tblSendChat)
    {
        NSDictionary *dict = [[arrForSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        if(([[dict valueForKey:@"receiver"] isEqualToString:strReceiverId] || [[dict valueForKey:@"receiver"] isEqualToString:staffNumber]) && ([[dict valueForKey:@"sender"] isEqualToString:strReceiverId] || [[dict valueForKey:@"sender"] isEqualToString:staffNumber]))
        {
            if([[dict valueForKey:@"sender"] isEqualToString:strReceiverId])
            {
                CellSendChat *cell = (CellSendChat *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
                return [cell getHeight];
            }
            else
            {
                CellReceiveChat *cell = (CellReceiveChat *) [self tableView:tableView cellForRowAtIndexPath:indexPath];
                return [cell cellHeight];
            }
        }
        else
        {
            return 0.0f;
        }
    }
    else
        return 52.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == self.tblSendChat)
        return 35.0f;
    else
        return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == self.tblSendChat)
    {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tblSendChat.bounds.size.width, 40)];
        UILabel *lblDate = [[UILabel alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - 50, 5, 100, 25)];
        lblDate.font = [UIFont fontWithName:@"HelveticaNeue" size:13.0f];
        lblDate.textAlignment = NSTextAlignmentCenter;
        NSString *strDate = [arrForDate objectAtIndex:section];
        NSString *current = //[self DateToString:[NSDate date] withFormate:@"yyyy-MM-dd"];
    }
}

        /// YesterDay Date Calulation

        /*NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = -1;
        NSDate *yesterday = [gregorian dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
        NSString *strYesterday=//[self DateToString:yesterday withFormate:@"yyyy-MM-dd"];*/

        /*NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        //[dateFormatter setTimeZone:(NSTimeZone *)[NSString stringWithFormat:@"%@",[dictInfo valueForKey:@"timezone"]]];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSDate *yourDate = [dateFormatter dateFromString:strDate];
        dateFormatter.dateFormat = @"dd";
        NSString *strDay= [dateFormatter stringFromDate:yourDate];
        NSString *strFullDay = nil;
        if ([strDay intValue]==1)
        {
            strFullDay = [NSString stringWithFormat:@"%@st",strDay];
        }
        if ([strDay intValue]==2)
        {
            strFullDay = [NSString stringWithFormat:@"%@nd",strDay];
        }
        if ([strDay intValue]==3)
        {
            strFullDay = [NSString stringWithFormat:@"%@rd",strDay];
        }
        if ([strDay intValue]>=4)
        {
            strFullDay = [NSString stringWithFormat:@"%@th",strDay];
        }*/

        /*if([strDate isEqualToString:current])
        {
            lblDate.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"TODAY", nil)];
            headerView.backgroundColor = [UIColor clearColor];
        }
        else if ([strDate isEqualToString:strYesterday])
        {
            lblDate.text = @"Yesterday";
            headerView.backgroundColor = [UIColor clearColor];
        }
        else
        {
            NSDate *date = //[self stringToDate:strDate withFormate:@"yyyy-MM-dd"];
            headerView.backgroundColor = [UIColor clearColor];
            NSString *text = //[self DateToString:date withFormate:@"dd MMM yyyy"];//02 Jan 2015
            lblDate.text = [NSString stringWithFormat:@"%@",text];
        }
        lblDate.layer.cornerRadius = 10.0f;
        lblDate.layer.masksToBounds = YES;
        lblDate.backgroundColor = [UIColor colorWithRed:135.0f/255.0f green:206.0f/255.0f blue:250.0f/255.0f alpha:1.0f];
        lblDate.textColor = [UIColor blackColor];
        [headerView addSubview:lblDate];
        return headerView;
    }
    else
        return nil;
}

#pragma mark
#pragma mark - UITextfield Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if([self.txtMessage isEqual:textField])
    {
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height);
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([self.txtMessage isEqual:textField])
    {
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height);
    }
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if([self.txtMessage isEqual:textField])
    {
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height);
    }
    [textField resignFirstResponder];

    if(self.txtMessage.text.length > 0)
    {
        self.btnSendMessage.enabled = YES;
        self.btnSendMessage.backgroundColor = [UIColor colorWithRed:0.0f/255.0f green:195.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
    }
    else
    {
        self.btnSendMessage.enabled = NO;
        self.btnSendMessage.backgroundColor = [UIColor lightGrayColor];
    }
}

#pragma mark
#pragma mark - UISegmentChanged Delegate

- (IBAction)segmentChanged:(UISegmentedControl *)sender
{
    segmentString = [sender titleForSegmentAtIndex:sender.selectedSegmentIndex];
    if([segmentString isEqualToString:@"Contact List"])
        [self getContactList];
    else
        [self.tableViewForContactList reloadData];
}

#pragma mark - chat

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIContentSizeCategoryDidChangeNotification object:nil];
}

- (void)didChangePreferredContentSize:(NSNotification *)notification
{
    [self.tblSendChat reloadData];
}

-(void)timerCalled
{
    if (timer==0)
    {}
    else
    {
        //[self getAllConverstion];
    }
}

#pragma mark-
#pragma mark - Table view data source

/*-(void)makeSection
{
    arrForDate = nil;
    arrForSection = nil;
    arrForDate=[[NSMutableArray alloc]init];
    arrForSection=[[NSMutableArray alloc]init];
    NSMutableArray *arrtemp=[[NSMutableArray alloc]init];

    for(int i=0; i<arrdata.count; i++)
    {
        NSMutableDictionary *dictChatData = [[NSMutableDictionary alloc]init];
        NSManagedObject *staffDuty = [arrdata objectAtIndex:i];
        [dictChatData setObject:[staffDuty valueForKey:@"content"] forKey:@"content"];
        [dictChatData setObject:[staffDuty valueForKey:@"contentReceiveTime"] forKey:@"contentReceiveTime"];
        if([staffDuty valueForKey:@"chatID"])
            [dictChatData setObject:[staffDuty valueForKey:@"chatID"] forKey:@"chatID"];
        else
            [dictChatData setObject:[staffDuty valueForKey:@"id"] forKey:@"chatID"];
        [dictChatData setObject:[staffDuty valueForKey:@"readDateTime"] forKey:@"readDateTime"];
        [dictChatData setObject:[staffDuty valueForKey:@"reads"] forKey:@"reads"];
        [dictChatData setObject:[staffDuty valueForKey:@"receiver"] forKey:@"receiver"];
        [dictChatData setObject:[staffDuty valueForKey:@"sendOutPush"] forKey:@"sendOutPush"];
        [dictChatData setObject:[staffDuty valueForKey:@"sendOutPushTime"] forKey:@"sendOutPushTime"];
        [dictChatData setObject:[staffDuty valueForKey:@"sender"] forKey:@"sender"];
        [arrtemp addObject:dictChatData];
    }

    // * when you want to ascending order sorting in date then must set YES on ascending property :

    NSSortDescriptor *distanceSortDiscriptor = [NSSortDescriptor sortDescriptorWithKey:@"contentReceiveTime" ascending:YES
                                                                              selector:@selector(localizedStandardCompare:)];

    [arrtemp sortUsingDescriptors:@[distanceSortDiscriptor]];

    for (int i=0; i<arrtemp.count; i++)
    {
        if(([[[arrtemp objectAtIndex:i] valueForKey:@"receiver"] isEqualToString:strReceiverId] || [[[arrtemp objectAtIndex:i] valueForKey:@"receiver"] isEqualToString:staffNumber]) && ([[[arrtemp objectAtIndex:i] valueForKey:@"sender"] isEqualToString:strReceiverId] || [[[arrtemp objectAtIndex:i] valueForKey:@"sender"] isEqualToString:staffNumber]))
        {
            NSMutableDictionary *dictDate=[[NSMutableDictionary alloc]init];
            dictDate=[arrtemp objectAtIndex:i];
            
            NSString *temp=[dictDate valueForKey:@"contentReceiveTime"];
            NSArray *arrDate=[temp componentsSeparatedByString:@" "];
            NSString *strdate=[arrDate objectAtIndex:0];
            if(![arrForDate containsObject:strdate])
            {
                [arrForDate addObject:strdate];
            }
        }
    }
    for (int j=0; j<arrForDate.count; j++)
    {
        NSMutableArray *a=[[NSMutableArray alloc]init];
        [arrForSection addObject:a];
    }
    for (int j=0; j<arrForDate.count; j++)
    {
        NSString *strTempDate=[arrForDate objectAtIndex:j];
        for (int i=0; i<arrtemp.count; i++)
        {
            NSMutableDictionary *dictSection=[[NSMutableDictionary alloc]init];
            dictSection=[arrtemp objectAtIndex:i];
            NSArray *arrDate=[[dictSection valueForKey:@"contentReceiveTime"] componentsSeparatedByString:@" "];
            NSString *strdate=[arrDate objectAtIndex:0];
            if ([strdate isEqualToString:strTempDate])
            {
                [[arrForSection objectAtIndex:j] addObject:dictSection];
            }
        }
    }
    [self.tblSendChat reloadData];
    if(arrForSection.count > 0)
    {
        self.tblSendChat.hidden = NO;
        self.noItem.hidden = YES;
        self.lblNoItem.hidden = YES;
        int lastRowNumber = [self.tblSendChat numberOfRowsInSection:0] - 1;
        NSIndexPath* ip = [NSIndexPath indexPathForRow:lastRowNumber inSection:0];
        [self.tblSendChat scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
    else
    {
        self.tblSendChat.hidden = YES;
        self.noItem.hidden = NO;
        self.lblNoItem.hidden = NO;
    }
}

#pragma mark - get last message

-(NSString *)getLastMessage:(NSString *)userStaffNumber
{
    [arrLastMessageData removeAllObjects];

    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Chat"];
    NSMutableArray *arrForChatData = [[NSMutableArray alloc]init];
    arrForChatData = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];

    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"contentReceiveTime" ascending:YES];
    NSArray *descriptors = [NSArray arrayWithObject: descriptor];
    NSArray *arrForAscendingChatData = [[NSMutableArray alloc]init];
    arrForAscendingChatData = [arrForChatData sortedArrayUsingDescriptors:descriptors];

    for(int i=0; i<arrForAscendingChatData.count; i++)
    {
        NSManagedObject *staffDuty = [arrForAscendingChatData objectAtIndex:i];
        if([[staffDuty valueForKey:@"receiver"] isEqualToString:userStaffNumber])
        {
            [arrLastMessageData addObject:[arrForAscendingChatData objectAtIndex:i]];
        }
    }
    NSString *strReturnValue = @"";
    if(arrLastMessageData.count > 0)
    {
        for(int j=0; j<arrLastMessageData.count; j++)
        {
            NSManagedObject *staffDuty = [arrLastMessageData objectAtIndex:j];
            strReturnValue = [staffDuty valueForKey:@"content"];
            NSString *temp = [staffDuty valueForKey:@"contentReceiveTime"];
            NSArray *arrDate = [temp componentsSeparatedByString:@" "];
            NSString *strTime = [arrDate objectAtIndex:1];
            NSArray *arrTime = [strTime componentsSeparatedByString:@":"];
            strLastMessageTime = [NSString stringWithFormat:@"%@:%@",[arrTime objectAtIndex:0],[arrTime objectAtIndex:1]];
        }
    }
    else
        strLastMessageTime = @"";

    return strReturnValue;
}

#pragma mark - get all chat conversation

- (void)getAllConverstion
{
    if([APPDELEGATE connected])
    {
        [[AppDelegate sharedAppDelegate]showHUDLoadingView:NSLocalizedString(@"Loading", nil)];
        
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:staffNumber forKey:PARAM_STAFF_NUMBER];
        
        AFBaseHandler *handler = [[AFBaseHandler alloc]init];
        [handler startHTTPRequestNoPressMessageHandlersURL:FILE_GET_ALL_CONVERSATION param:dictParam successBlock:^(id response)
         {
             [[AppDelegate sharedAppDelegate]hideHUDLoadingView];
             [arrdata removeAllObjects];
             NSLog(@"get all conversation response = %@",response);
             if([[response valueForKey:@"msg"] isEqualToString:@"success"])
             {
                 [arrdata addObjectsFromArray:[response valueForKey:@"json"]];
                 if(arrdata==nil||arrdata.count==0)
                 {
                     self.tblSendChat.hidden=YES;
                     self.noItem.hidden=NO;
                     self.lblNoItem.hidden=NO;
                 }
                 else
                 {
                     self.noItem.hidden=YES;
                     self.lblNoItem.hidden=YES;
                     for(int i=0; i<arrdata.count; i++)
                     {
                         NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
                         NSManagedObject *newDevice = [NSEntityDescription insertNewObjectForEntityForName:@"Chat" inManagedObjectContext:context];
                         [newDevice setValue:[NSString stringWithFormat:@"%@",[[arrdata objectAtIndex:i]valueForKey:@"content"]] forKey:@"content"];
                         [newDevice setValue:[NSString stringWithFormat:@"%@",[[arrdata objectAtIndex:i]valueForKey:@"contentReceiveTime"]] forKey:@"contentReceiveTime"];
                         [newDevice setValue:[NSString stringWithFormat:@"%@",[[arrdata objectAtIndex:i]valueForKey:@"id"]] forKey:@"chatID"];
                         [newDevice setValue:[NSString stringWithFormat:@"%@",[[arrdata objectAtIndex:i]valueForKey:@"readDateTime"]] forKey:@"readDateTime"];
                         [newDevice setValue:[NSString stringWithFormat:@"%@",[[arrdata objectAtIndex:i]valueForKey:@"reads"]] forKey:@"reads"];
                         [newDevice setValue:[NSString stringWithFormat:@"%@",[[arrdata objectAtIndex:i]valueForKey:@"receiver"]] forKey:@"receiver"];
                         [newDevice setValue:[NSString stringWithFormat:@"%@",[[arrdata objectAtIndex:i]valueForKey:@"sendOutPush"]] forKey:@"sendOutPush"];
                         [newDevice setValue:[NSString stringWithFormat:@"%@",[[arrdata objectAtIndex:i]valueForKey:@"sendOutPushTime"]] forKey:@"sendOutPushTime"];
                         [newDevice setValue:[NSString stringWithFormat:@"%@",[[arrdata objectAtIndex:i]valueForKey:@"sender"]] forKey:@"sender"];
                         
                         NSError *error = nil;
                         // Save the object to persistent store
                         if (![context save:&error])
                             NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
                     }
                     [arrdata removeAllObjects];
                     NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
                     NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Chat"];
                     NSMutableArray *arrForChatData = [[NSMutableArray alloc]init];
                     arrForChatData = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                     
                     NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"contentReceiveTime" ascending:YES];
                     NSArray *descriptors = [NSArray arrayWithObject: descriptor];
                     NSArray *arrForAscendingChatData = [[NSMutableArray alloc]init];
                     arrForAscendingChatData = [arrForChatData sortedArrayUsingDescriptors:descriptors];
                     
                     for(int i=0; i<arrForAscendingChatData.count; i++)
                     {
                         [arrdata addObject:[arrForAscendingChatData objectAtIndex:i]];
                     }
                     if(arrdata.count > 0)
                         [self makeSection];
                 }
             }
             else
             {
                 NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
                 NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Chat"];
                 NSMutableArray *arrForChatData = [[NSMutableArray alloc]init];
                 arrForChatData = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                 
                 NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"contentReceiveTime" ascending:YES];
                 NSArray *descriptors = [NSArray arrayWithObject: descriptor];
                 NSArray *arrForAscendingChatData = [[NSMutableArray alloc]init];
                 arrForAscendingChatData = [arrForChatData sortedArrayUsingDescriptors:descriptors];
                 
                 for(int i=0; i<arrForAscendingChatData.count; i++)
                 {
                     [arrdata addObject:[arrForAscendingChatData objectAtIndex:i]];
                 }
                 if(arrdata.count > 0)
                     [self makeSection];
             }
             self.viewForChat.hidden = NO;
         }
        failerBlock:^(id obj)
         {
             [[AppDelegate sharedAppDelegate]hideHUDLoadingView];
             NSLog(@"error device add url = %@",obj);
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error!" message:NSLocalizedString(@"Internet Connection Error", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
        alert.tag = 101;
        [alert show];
    }
}

#pragma mark - Fetch Data From DB

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark - send chat message

-(void)sendChat
{
    if([APPDELEGATE connected])
    {
        [[AppDelegate sharedAppDelegate]showHUDLoadingView:NSLocalizedString(@"Loading", nil)];

        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:staffNumber forKey:PARAM_STAFF_NUMBER];
        [dictParam setObject:strReceiverId forKey:PARAM_RECEIVER];
        [dictParam setObject:self.txtMessage.text forKey:PARAM_CONTENT];

        AFBaseHandler *handler = [[AFBaseHandler alloc]init];
        [handler startHTTPRequestNoPressMessageHandlersURL:FILE_SEND_MESSAGE param:dictParam successBlock:^(id response)
        {
            [[AppDelegate sharedAppDelegate]hideHUDLoadingView];
            NSLog(@"send message response = %@",response);
            if([[response valueForKey:@"msg"] isEqualToString:@"success"])
            {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
                NSString *currentDate = [dateFormatter stringFromDate:[NSDate date]];

                NSManagedObjectContext *context = [APPDELEGATE managedObjectContext];
                NSManagedObject *newDevice = [NSEntityDescription insertNewObjectForEntityForName:@"Chat" inManagedObjectContext:context];
                [newDevice setValue:[NSString stringWithFormat:@"%@",self.txtMessage.text] forKey:@"content"];
                [newDevice setValue:[NSString stringWithFormat:@"%@",currentDate] forKey:@"contentReceiveTime"];
                [newDevice setValue:[NSString stringWithFormat:@"%@",@""] forKey:@"chatID"];
                [newDevice setValue:[NSString stringWithFormat:@"%@",@""] forKey:@"readDateTime"];
                [newDevice setValue:[NSString stringWithFormat:@"%@",@""] forKey:@"reads"];
                [newDevice setValue:[NSString stringWithFormat:@"%@",strReceiverId] forKey:@"receiver"];
                [newDevice setValue:[NSString stringWithFormat:@"%@",@""] forKey:@"sendOutPush"];
                [newDevice setValue:[NSString stringWithFormat:@"%@",@""] forKey:@"sendOutPushTime"];
                [newDevice setValue:[NSString stringWithFormat:@"%@",staffNumber] forKey:@"sender"];

                NSError *error = nil;
                // Save the object to persistent store
                if (![context save:&error])
                    NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);

                self.txtMessage.text = @"";
                self.btnSendMessage.enabled = NO;
                self.btnSendMessage.backgroundColor = [UIColor lightGrayColor];
                [self.txtMessage resignFirstResponder];
                [self.tableViewForContactList reloadData];
                [self getAllConverstion];
            }
            else
            {
                [APPDELEGATE showToastMessage:NSLocalizedString(@"Message Sent Failure", nil)];
            }
        }
        failerBlock:^(id obj)
        {
            [[AppDelegate sharedAppDelegate]hideHUDLoadingView];
            NSLog(@"error device add url = %@",obj);
        }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error!" message:NSLocalizedString(@"Internet Connection Error", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
        //alert.tag = 101;
        [alert show];
    }
}

#pragma mark - on click method of chat

- (IBAction)onClickforBack:(id)sender
{
    timer=0;
    self.viewForChat.hidden = YES;
}

- (IBAction)onClickforSend:(id)sender
{
    [self sendChat];
}

#pragma mark - Date Formatter Methods

- (NSDate *)stringToDate:(NSString *)dateString withFormate:(NSString *)format
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSDate *date = [dateFormatter dateFromString: dateString];
    return date;
}

-(NSString *)DateToString:(NSDate *)date withFormate:(NSString *)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];//2013-07-15:10:00:00
    NSString * strdate = [formatter stringFromDate:date];
    return strdate;
}

#pragma mark - Memory Method

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}*/

@end