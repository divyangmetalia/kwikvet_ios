//
//  ChatContactListCell.h
//  OceanPark
//
//  Created by My Mac on 11/6/15.
//  Copyright (c) 2015 Elluminati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatContactListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageViewForChatContact;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@end