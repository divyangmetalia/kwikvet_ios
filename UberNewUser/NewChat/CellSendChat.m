//
//  CellSendChat.m
//  TrocaTroca
//
//  Created by Rahul on 16/08/16.
//  Copyright © 2016 Elluminati. All rights reserved.

#import "CellSendChat.h"

@implementation CellSendChat

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.lblChat setTextColor:[UIColor blackColor]];
}

- (void)setCellData:(NSDictionary *)dictData withParent:(id)parent
{
    if(dictData != nil)
    {
        self.viewInner.hidden = NO;
        cellParent = parent;
        NSString *str = [dictData valueForKey:@"contentReceiveTime"];
        NSArray *ar = [str componentsSeparatedByString:@" "];
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"hh:mm:ss"];
        NSDate *date = [format dateFromString:[ar objectAtIndex:1]];
        [format setDateFormat:@"hh:mm"];
        NSString *strText = [NSString stringWithFormat:@"%@",[dictData valueForKey:@"content"]];
        NSString *trimmed = [strText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        self.lblChat.text = trimmed;
        self.lblDate.text = [NSString stringWithFormat:@"%@",[format stringFromDate:date]];
        [self getHeight];
    }
    else
    {
        self.viewInner.hidden = YES;
    }
}

- (float) getHeight
{
    UIViewController *vc = (UIViewController *)cellParent;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, vc.view.frame.size.width, self.frame.size.height);

    float height = 0.0f;

    CGRect rect = self.lblChat.frame;
    rect.origin.y = 8;
    rect.size.width = self.frame.size.width - (16*2);
    rect.size.height = CGFLOAT_MAX;
    self.lblChat.frame = rect;

    [self.lblChat sizeToFit];

    rect = self.lblChat.frame;
    rect.origin.y = 8;
    rect.size.width = self.frame.size.width - (16*6+4);
    self.lblChat.frame = rect;

    rect = self.lblDate.frame;
    rect.size.width = self.frame.size.width - (16*6+4);
    rect.origin.y = self.lblChat.frame.origin.y + self.lblChat.frame.size.height + 7;
    self.lblDate.frame = rect;

    rect = self.viewInner.frame;
    rect.size.width = self.frame.size.width - 16;
    rect.size.height = self.lblDate.frame.origin.y + self.lblDate.frame.size.height + 8;
    self.viewInner.frame = rect;

    height = self.viewInner.frame.size.height + 16;

    self.imageForChatBackground.frame = CGRectMake(self.imageForChatBackground.frame.origin.x, self.imageForChatBackground.frame.origin.y, self.imageForChatBackground.frame.size.width, self.viewInner.frame.size.height);

    return height;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end