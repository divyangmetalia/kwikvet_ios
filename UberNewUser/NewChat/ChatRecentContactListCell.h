//
//  ChatRecentContactListCell.h
//  OceanPark
//
//  Created by Elluminati Mac Mini 1 on 02/09/16.
//  Copyright © 2016 KingY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatRecentContactListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageViewForChatContact;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblLastMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblLastMessageTime;

@end