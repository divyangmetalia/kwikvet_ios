//
//  CellReceiveChat.m
//  TrocaTroca
//
//  Created by Rahul on 16/08/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import "CellReceiveChat.h"

@implementation CellReceiveChat

- (void)awakeFromNib
{
    [super awakeFromNib];
}

-(CGFloat)getCellHeightFromContent:(NSMutableDictionary *)dictData
{
    NSString *str1 =[dictData valueForKey:@"contentReceiveTime"];
    NSArray *ar1=[str1 componentsSeparatedByString:@" "];
    NSDateFormatter *format=[[NSDateFormatter alloc] init];
    [format setDateFormat:@"hh:mm:ss"];
    [format setDateFormat:@"hh:mm"];
    NSString *strText1=[NSString stringWithFormat:@"%@\n%@",[dictData valueForKey:@"content"],[ar1 objectAtIndex:1]];
    CGSize constraintO1= CGSizeMake(self.lblSended.frame.size.width, MAXFLOAT);
    NSStringDrawingContext *contextO1 = [[NSStringDrawingContext alloc] init];
    CGSize boundingBoxO1 = [strText1 boundingRectWithSize:constraintO1
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Avenir Book" size:17.0]}
                                                  context:contextO1].size;
    CGSize labelSizeO1 = CGSizeMake(ceil(boundingBoxO1.width), ceil(boundingBoxO1.height));
    return labelSizeO1.height;
    return 50.0f;
}

-(void)setRecivedChatData:(NSDictionary *)dictChatData strImageForSelller:(NSString *)strSellerImage perent:(id)perent
{
    perentVC = perent;
    NSString *str = [dictChatData valueForKey:@"contentReceiveTime"];
    NSArray *ar = [str componentsSeparatedByString:@" "];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"hh:mm:ss"];
    NSDate *date = [format dateFromString:[ar objectAtIndex:1]];
    [format setDateFormat:@"hh:mm"];
    NSString *strText = [NSString stringWithFormat:@"%@",[dictChatData valueForKey:@"content"]];
    self.lblSended.text = strText;
    self.lblSendDate.text = [NSString stringWithFormat:@"%@",[format stringFromDate:date]];
    //[self.imgSeller applyRoundedCornersFull];
    //[self.imgSeller downloadFromURL:[NSString stringWithFormat:@"%@",strSellerImage] withPlaceholder:[UIImage imageNamed:@"demo"]];
    self.imgSeller.layer.cornerRadius = self.imgSeller.frame.size.width / 2;
    self.imgSeller.layer.cornerRadius = self.imgSeller.frame.size.height / 2;
    self.imgSeller.clipsToBounds = YES;
    [self cellHeight];
}

-(float)cellHeight
{
    UIViewController *vc = (UIViewController *)perentVC;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, vc.view.frame.size.width, self.frame.size.height);

    float height = 0.0f;

    CGRect rect = self.lblSended.frame;
    rect.origin.y = 8;
    rect.size.width = self.frame.size.width - 100;
    rect.size.height = CGFLOAT_MAX;
    self.lblSended.frame = rect;

    [self.lblSended sizeToFit];

    rect = self.lblSended.frame;
    rect.origin.y = 8;
    rect.size.width = self.frame.size.width - 100;
    self.lblSended.frame = rect;

    rect = self.lblSendDate.frame;
    rect.size.width = self.frame.size.width - 100;
    rect.origin.y = self.lblSended.frame.origin.y + self.lblSended.frame.size.height +7;
    self.lblSendDate.frame = rect;

    rect = self.inerView.frame;
    rect.size.width = self.frame.size.width - 84;
    rect.size.height = self.lblSendDate.frame.origin.y + self.lblSendDate.frame.size.height + 8;
    self.inerView.frame = rect;

    height = self.inerView.frame.size.height + 16;

    self.imageForChatBackground.frame = CGRectMake(self.imageForChatBackground.frame.origin.x, self.imageForChatBackground.frame.origin.y, self.imageForChatBackground.frame.size.width, self.inerView.frame.size.height);

    return height;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end