//
//  CellReceiveChat.h
//  TrocaTroca
//
//  Created by Rahul on 16/08/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellReceiveChat : UITableViewCell
{
    id perentVC;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgSeller;
@property (weak, nonatomic) IBOutlet UILabel *lblSended;
@property (weak, nonatomic) IBOutlet UILabel *lblSendDate;
@property (weak, nonatomic) IBOutlet UIView *inerView;
@property (weak, nonatomic) IBOutlet UIImageView *imageForChatBackground;

-(float)cellHeight;
-(void)setRecivedChatData:(NSDictionary *)dictChatData strImageForSelller:(NSString *)strSellerImage perent:(id)perent;

@end