//
//  HistoryDetailVC.h
//  Rider Driver
//
//  Created by My Mac on 7/8/15.
//  Copyright (c) 2015 Elluminati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

@interface HistoryDetailVC : BaseVC<UIAlertViewDelegate,UIGestureRecognizerDelegate>
{
    NSString *strID;
}

@property (strong,nonatomic) NSDictionary *dictInfo;
@property (strong, nonatomic) NSString *type;

@property (weak, nonatomic) IBOutlet UIButton *btnNavigationTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnChat;

- (IBAction)btnBackPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *imgFullImage;

@property (weak, nonatomic) IBOutlet UIView *viewForlLagreImage;

@property (weak, nonatomic) IBOutlet UIButton *btnClose;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewForLab;
@property (weak, nonatomic) IBOutlet UILabel *lblLabResult;
@property (weak, nonatomic) IBOutlet UIImageView *noItemsForLab;
- (IBAction)onClickCloseView:(id)sender;

@end