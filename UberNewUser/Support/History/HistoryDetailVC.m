//
//  HistoryDetailVC.m
//  Rider Driver
//
//  Created by My Mac on 7/8/15.
//  Copyright (c) 2015 Elluminati. All rights reserved.
//

#import "HistoryDetailVC.h"
#import "AFNHelper.h"
#import "UIView+Utils.h"
#import "UIImageView+Download.h"
#import "CollectionCell.h"
#import "ChatVC.h"

@interface HistoryDetailVC ()
{
    NSMutableArray *arrForLabResult;
    NSInteger tag;
}

@end

@implementation HistoryDetailVC
@synthesize type;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
    self.navigationItem.hidesBackButton = YES;
    [self customeSetup];
    [self setLocalization];
    [self setTripDetails];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.viewForlLagreImage addGestureRecognizer:swipeLeft];
    [self.viewForlLagreImage addGestureRecognizer:swipeRight];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - custom method

-(void)handleSwipe:(UISwipeGestureRecognizer *)sender
{
    
    UISwipeGestureRecognizerDirection direction = [(UISwipeGestureRecognizer *)sender direction];
    
    switch (direction)
    {
        case UISwipeGestureRecognizerDirectionLeft:
            tag++;
            break;
        case UISwipeGestureRecognizerDirectionRight:
            tag--;
            break;
        default:
            break;
    }
    
    tag = (tag < 0 )? ([arrForLabResult count] -1):
    tag % [arrForLabResult count];
    
    UICollectionView *clView = (UICollectionView*)[(UIGestureRecognizer *)sender view];
    NSLog(@"tap: %@", clView);
    NSLog(@"tag: %ld", (long)clView.tag);
    
    
    [self.imgFullImage downloadFromURL:[[arrForLabResult objectAtIndex:tag] valueForKey:@"document"] withPlaceholder:[UIImage imageNamed:@"user"]];
}

-(void)customeSetup
{
    self.viewForlLagreImage.hidden=YES;
    self.noItemsForLab.hidden=YES;
    
    arrForLabResult = [[NSMutableArray alloc] init];
    
    self.btnChat.titleLabel.textAlignment = NSTextAlignmentCenter;
}

-(void)setLocalization
{
    self.lblLabResult.text = NSLocalizedString(@"LAB_RESULT", nil);
    
    [self.btnClose setTitle:NSLocalizedString(@"Close", nil) forState:UIControlStateNormal];
    [self.btnChat setTitle:NSLocalizedString(@"CHAT_WITH_PROVIDER", nil) forState:UIControlStateNormal];
}

-(void)setTripDetails
{
    [arrForLabResult addObjectsFromArray:[self.dictInfo valueForKey:@"lab_result"]];
    
    if(arrForLabResult.count>0)
        [self.collectionViewForLab reloadData];
    else
        self.noItemsForLab.hidden=NO;
    
    [APPDELEGATE hideLoadingView];
}

#pragma mark -
#pragma mark - ColetionView Methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  arrForLabResult.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionCell *cell = [self.collectionViewForLab dequeueReusableCellWithReuseIdentifier:@"collectionCell" forIndexPath:indexPath];
    
    [cell.imgForLab downloadFromURL:[[arrForLabResult objectAtIndex:indexPath.row] valueForKey:@"document"] withPlaceholder:[UIImage imageNamed:@"user"]];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.imgFullImage downloadFromURL:[[arrForLabResult objectAtIndex:indexPath.row] valueForKey:@"document"] withPlaceholder:[UIImage imageNamed:@"user"]];
    
    tag = indexPath.row;
    self.viewForlLagreImage.hidden=NO;
}

#pragma mark -
#pragma mark - Button Action Methods

- (IBAction)onClickCloseView:(id)sender
{
    self.viewForlLagreImage.hidden=YES;
}
- (IBAction)btnBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma  mark - 
#pragma  mark -  prepare for segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"segueToChat"])
    {
        ChatVC *vc = [segue destinationViewController];
        vc.dictHistoryInfo = self.dictInfo;
    }
}

@end