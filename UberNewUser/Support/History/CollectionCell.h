//
//  CollectionCell.h
//  KwikVet for VETS
//
//  Created by Elluminati Macbook Pro 2 on 10/13/16.
//  Copyright © 2016 Deep Gami. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgForLab;

@end
