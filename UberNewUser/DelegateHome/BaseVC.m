//
//  BaseVC.m
//  Employee
//
//  Created by Raj Oriya on 19/05/14.
//  Copyright (c) 2014 Elluminati MacBook Pro 1. All rights reserved.
//

#import "BaseVC.h"


@interface BaseVC ()

@end

@implementation BaseVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    animPop=YES;
    /*
    UIButton *btnLeft=[UIButton buttonWithType:UIButtonTypeCustom];
    btnLeft.frame=CGRectMake(0, 0, 18, 16);
    [btnLeft setImage:[UIImage imageNamed:@"button_back"] forState:UIControlStateNormal];
    [btnLeft addTarget:self action:@selector(leftDrawerButtonPress:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:btnLeft];
     */
    /*
    if (ISIOS7) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
    */
}
/*
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
*/

#pragma mark -
#pragma mark - Utility Methods

-(void)setNavBarTitle:(NSString *)title
{
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150, 30)];
    lbl.textAlignment=NSTextAlignmentCenter;
    lbl.backgroundColor=[UIColor clearColor];
    lbl.font=[UIFont systemFontOfSize:16.0];
    lbl.textColor=[UIColor whiteColor];
    lbl.text=title;
    //self.navigationItem.titleView=lbl;
}

-(void)setBackBarItem
{
    self.navigationItem.hidesBackButton = YES;
    //self.navigationItem.backBarButtonItem = nil;
    UIButton *btnLeft=[UIButton buttonWithType:UIButtonTypeCustom];
    btnLeft.frame=CGRectMake(0, 0, 25, 25);
    //btnLeft.frame=CGRectMake(0, 0, 18, 16);
    //[btnLeft setImage:[UIImage imageNamed:@"icon_header"] forState:UIControlStateNormal];
    //[btnLeft setTitle:@"Back" forState:UIControlStateNormal];
    //[btnLeft setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btnLeft addTarget:self action:@selector(onClickBackBarItem:) forControlEvents:UIControlEventTouchUpInside];
    //self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:btnLeft];
    [btnLeft setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnLeft];
}

-(void)setBackBarItemPresent
{
    self.navigationItem.hidesBackButton = YES;
    //self.navigationItem.backBarButtonItem = nil;
    UIButton *btnLeft=[UIButton buttonWithType:UIButtonTypeCustom];
    btnLeft.frame=CGRectMake(0, 0, 25, 25);
    //btnLeft.frame=CGRectMake(0, 0, 18, 16);
    //[btnLeft setImage:[UIImage imageNamed:@"icon_header"] forState:UIControlStateNormal];
    //[btnLeft setTitle:@"Back" forState:UIControlStateNormal];
    //[btnLeft setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btnLeft addTarget:self action:@selector(onClickBackBarItemDissmiss:) forControlEvents:UIControlEventTouchUpInside];
    //self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:btnLeft];
    [btnLeft setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnLeft];
}

-(void)setBackBarItem:(BOOL)animated
{
    animPop=animated;
    [self setBackBarItem];
}


-(void)onClickBackBarItem:(id)sender
{
    [self.navigationController popViewControllerAnimated:animPop];
}

-(void)onClickBackBarItemDissmiss:(id)sender
{
    [self.navigationController dismissModalViewControllerAnimated:animPop];
}

#pragma mark
#pragma mark - Remove Null

- (NSMutableDictionary *)cleanNullInJsonDic:(NSMutableDictionary *)dic
{
    if (!dic || (id)dic == [NSNull null])
    {
        return dic;
    }
    NSMutableDictionary *mulDic = [[NSMutableDictionary alloc] init];
    for (NSString *key in [dic allKeys])
    {
        NSObject *obj = dic[key];
        if (!obj || obj == [NSNull null])
        {
            [mulDic setObject:@"N/A" forKey:key];
        }else if ([obj isKindOfClass:[NSDictionary class]])
        {
            [mulDic setObject:[self cleanNullInJsonDic:(NSMutableDictionary *)obj] forKey:key];
        }else if ([obj isKindOfClass:[NSArray class]])
        {
            NSMutableArray *array = [self cleanNullInJsonArray:(NSMutableArray *)obj];
            [mulDic setObject:array forKey:key];
        }else
        {
            [mulDic setObject:obj forKey:key];
        }
    }
    return mulDic;
}


- (NSMutableArray *)cleanNullInJsonArray:(NSMutableArray *)array
{
    if (!array || (id)array == [NSNull null])
    {
        return array;
    }
    NSMutableArray *mulArray = [[NSMutableArray alloc] init];
    for (NSObject *obj in array)
    {
        if (!obj || obj == [NSNull null])
        {
            [mulArray addObject:@"N/A"];
        }else if ([obj isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *dic = [self cleanNullInJsonDic:(NSMutableDictionary *)obj];
            [mulArray addObject:dic];
        }else if ([obj isKindOfClass:[NSArray class]])
        {
            NSArray *a = [self cleanNullInJsonArray:(NSMutableArray *)obj];
            [mulArray addObject:a];
        }else
        {
            [mulArray addObject:obj];
        }
    }
    return mulArray;
}

#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end