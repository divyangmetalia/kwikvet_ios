//
//  FeedBackVC.h
//  UberNewUser
//
//  Created by Elluminati on 01/11/14.
//  Copyright (c) 2014 Elluminati. All rights reserved.
//

#import "BaseVC.h"
#import "RatingBar.h"
#import "SWRevealViewController.h"

@interface FeedBackVC : BaseVC<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    RatingBar *ratingView;
}

//////////// Outlets Price Label
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentMode;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentModeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (nonatomic,strong) NSString *strUserImg;
@property (nonatomic,strong) NSMutableDictionary *dictWalkInfo;
@property (nonatomic,strong) NSString *strFirstName;
@property (nonatomic, strong) NSString *strLastName;
@property (nonatomic, assign) int isAccessToVet;

@property (weak, nonatomic) IBOutlet UITextView *txtComments;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;

@property (weak, nonatomic) IBOutlet UIView *viewForBill;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnFeedBack;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstName;
@property (weak, nonatomic) IBOutlet UILabel *lblLastName;

@property (weak, nonatomic) IBOutlet UILabel *lTotalCost;
@property (weak, nonatomic) IBOutlet UILabel *lblInvoice;
@property (weak, nonatomic) IBOutlet UILabel *lComment;
@property (weak, nonatomic) IBOutlet UITableView *tableForTypes;


- (IBAction)onClickBackBtn:(id)sender;

- (IBAction)submitBtnPressed:(id)sender;
- (IBAction)confirmBtnPressed:(id)sender;
@end
