//
//  FeedBackVC.m
//  UberNewUser
//
//  Created by Elluminati on 01/11/14.
//  Copyright (c) 2014 Elluminati. All rights reserved.
//

#import "FeedBackVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "PickUpVC.h"
#import "UberStyleGuide.h"
#import "subTypeCell.h"

@interface FeedBackVC ()
{
    NSString *strForCurrency;
    
    NSMutableArray *arrSubTypes;
}

@end

@implementation FeedBackVC

#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self SetLocalization];
    self.navigationController.navigationBarHidden=NO;
    NSArray *arrName=[self.strFirstName componentsSeparatedByString:@" "];
    self.lblFirstName.text=[NSString stringWithFormat:@"%@ %@",[arrName objectAtIndex:0],[arrName objectAtIndex:1]];

    [self.imgUser applyRoundedCornersFull];
    [self.imgUser downloadFromURL:self.strUserImg withPlaceholder:nil];
    self.viewForBill.hidden=NO;
    self.txtComments.text=NSLocalizedString(@"COMMENT", nil);
    [self customFont];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setPriceValue];
    
    arrSubTypes = [[NSMutableArray alloc]init];
    [arrSubTypes addObjectsFromArray:[dictBillInfo valueForKey:@"type"]];
    [arrSubTypes addObjectsFromArray:[dictBillInfo valueForKey:@"normal_order"]];
    [arrSubTypes addObjectsFromArray:[dictBillInfo valueForKey:@"remote_order"]];
    [arrSubTypes addObjectsFromArray:[dictBillInfo valueForKey:@"followup_order"]];
    
    strForCurrency = [dictBillInfo valueForKey:@"currency"];
    
    NSMutableDictionary *dictprice = [[NSMutableDictionary alloc]init];
    
    dictprice = [[NSMutableDictionary alloc]init];
    [dictprice setValue:NSLocalizedString(@"PROMO BOUNCE", nil) forKey:@"name"];
    [dictprice setValue:[dictBillInfo valueForKey:@"promo_bonus"] forKey:@"price"];
    [arrSubTypes addObject:dictprice];
    
    dictprice = [[NSMutableDictionary alloc]init];
    [dictprice setValue:NSLocalizedString(@"REFERRAL BOUNCE", nil) forKey:@"name"];
    [dictprice setValue:[dictBillInfo valueForKey:@"referral_bonus"] forKey:@"price"];
    [arrSubTypes addObject:dictprice];
    [self customSetup];
}

- (void)viewDidAppear:(BOOL)animated
{
   // [self customSetup];
    [[AppDelegate sharedAppDelegate] hideLoadingView];
    [self.btnFeedBack setTitle:NSLocalizedString(@"Invoice", nil) forState:UIControlStateNormal];
}

-(void)SetLocalization
{
    self.lblInvoice.text=NSLocalizedString(@"Invoice", nil);
    self.lTotalCost.text=NSLocalizedString(@"Total Due", nil);
    self.lComment.text=NSLocalizedString(@"COMMENT1", nil);
    self.lblPaymentModeTitle.text=NSLocalizedString(@"Payment Mode", nil);
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController)
    {
        [self.btnFeedBack addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    }
}
-(void)confirmFormerVetResponse:(NSString *)response
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSString *strForUserId=[PREF objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[PREF objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[PREF objectForKey:PREF_REQ_ID];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:strForUserId forKey:PARAM_ID];
        [dict setValue:strForUserToken forKey:PARAM_TOKEN];
        [dict setValue:strReqId forKey:PARAM_REQUEST_ID];
        [dict setValue:response forKey:@"is_confirm"];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_FORMER_VET withParamData:dict withBlock:^(id response, NSError *error)
         {
             NSLog(@"Former Vet--->%@",response);
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     [APPDELEGATE hideLoadingView];
                     [self.btnFeedBack setTitle:NSLocalizedString(@"Feedback", nil) forState:UIControlStateNormal];
                     self.viewForBill.hidden=YES;
                     self.navigationController.navigationBarHidden=NO;
                     
                     ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(150, 27) AndPosition:CGPointMake(85, 210)];
                     ratingView.backgroundColor=[UIColor clearColor];
                     [self.view addSubview:ratingView];
                 }
                 else
                 {
                     if ([[response valueForKey:@"error_code"] integerValue]==406)
                     {
                         [self performSegueWithIdentifier:@"logout" sender:self];
                     }
                 }
             }
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}


#pragma mark-
#pragma mark- Set Invoice Details

-(void)setPriceValue
{
    self.lblTotal.text=[NSString stringWithFormat:@"$ %.2f",[[dictBillInfo valueForKey:@"total"] floatValue]];
    
    if([[dictBillInfo valueForKey:@"payment_type"]intValue]==1)
    {
        self.lblPaymentMode.text = NSLocalizedString(@"Cash Payment", nil);
    }
    else
    {
        self.lblPaymentMode.text = NSLocalizedString(@"Card Payment", nil);
    }
}

#pragma mark-
#pragma mark- Custom Font

-(void)customFont
{
    self.lblTotal.font=[UberStyleGuide fontRegularLight:42.13f];
    self.lblFirstName.font=[UberStyleGuide fontRegularLight:20.0f];
    self.lblLastName.font=[UberStyleGuide fontRegularLight:20.0f];
    self.btnSubmit=[APPDELEGATE setBoldFontDiscriptor:self.btnSubmit];

    self.btnConfirm.titleLabel.font=[UberStyleGuide fontRegularBold];
    self.btnSubmit.titleLabel.font=[UberStyleGuide fontRegularBold];
}

#pragma mark-
#pragma mark- TableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSubTypes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"subType";
    
    subTypeCell *cell = [self.tableForTypes dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell==nil)
    {
        cell=[[subTypeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary *dict = [arrSubTypes objectAtIndex:indexPath.row];
    
    cell.lblTypeName.text = [dict valueForKey:@"name"];
    cell.lblPrice.text =[NSString stringWithFormat:@"%@ %.2f",strForCurrency,[[dict valueForKey:@"price"] floatValue]];
    
    return cell;
}

#pragma mark-
#pragma makr- Btn Click Events

- (IBAction)onClickBackBtn:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)submitBtnPressed:(id)sender
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REVIEWING", nil)];
        
        [self.txtComments resignFirstResponder];
        RBRatings rating=[ratingView getcurrentRatings];
        float rate=rating/2.0;
        if (rating%2 != 0)
        {
            rate += 0.5;
        }
        if(rate==0)
        {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"PLEASE_RATE", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        }
        else
        {
            NSString *strForUserId=[PREF objectForKey:PREF_USER_ID];
            NSString *strForUserToken=[PREF objectForKey:PREF_USER_TOKEN];
            NSString *strReqId=[PREF objectForKey:PREF_REQ_ID];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:strForUserId forKey:PARAM_ID];
            [dictParam setObject:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setObject:strReqId forKey:PARAM_REQUEST_ID];
            [dictParam setObject:[NSString stringWithFormat:@"%f",rate] forKey:PARAM_RATING];
            NSString *commt=self.txtComments.text;
            if([commt isEqualToString:NSLocalizedString(@"COMMENT", nil)])
            {
                [dictParam setObject:@"" forKey:PARAM_COMMENT];
            }
            else
            {
                [dictParam setObject:self.txtComments.text forKey:PARAM_COMMENT];
            }
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_RATE_DRIVER withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 NSLog(@"%@",response);
                 if (response)
                 {
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"RATING", nil)];
                         [PREF removeObjectForKey:PREF_REQ_ID];
                         [PREF synchronize];
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }
                     else
                     {
                         if ([[response valueForKey:@"error_code"] integerValue]==406)
                         {
                             [self performSegueWithIdentifier:@"logout" sender:self];
                         }
                     }
                 }
                 
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 
             }];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

#pragma mark -
#pragma mark - UITextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(0, -150, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        
    } completion:^(BOOL finished)
     {
     }];
  
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)confirmBtnPressed:(id)sender
{
    if(self.isAccessToVet==1)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"ALLOW_FORMER_VET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"No", nil) otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
        alert.tag=444;
        [alert show];
    }
    else
    {
        [self.btnFeedBack setTitle:NSLocalizedString(@"Feedback", nil) forState:UIControlStateNormal];
        self.viewForBill.hidden=YES;
        self.navigationController.navigationBarHidden=NO;

        ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(150, 27) AndPosition:CGPointMake(85, 210)];
        ratingView.backgroundColor=[UIColor clearColor];
        [self.view addSubview:ratingView];
    }
}

#pragma mark-
#pragma mark- Alert Button Clicked Event

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==444)
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        if(buttonIndex==0)
            [self confirmFormerVetResponse:@"0"];
        else
            [self confirmFormerVetResponse:@"1"];
    }
}

#pragma mark-
#pragma mark- Text Field Delegate

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtComments resignFirstResponder];
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.txtComments.text=@"";
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComments)
            {
                UITextPosition *beginning = [self.txtComments beginningOfDocument];
                [self.txtComments setSelectedTextRange:[self.txtComments textRangeFromPosition:beginning
                                                                                  toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComments)
            {
                UITextPosition *beginning = [self.txtComments beginningOfDocument];
                [self.txtComments setSelectedTextRange:[self.txtComments textRangeFromPosition:beginning
                                                                                  toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComments)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComments)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
    if ([self.txtComments.text isEqualToString:@""])
    {
        self.txtComments.text=NSLocalizedString(@"COMMENT", nil);
    }
}

#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
