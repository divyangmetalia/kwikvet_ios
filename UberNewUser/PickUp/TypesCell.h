//
//  TypesCell.h
//  KwikVet for VETS
//
//  Created by Elluminati Macbook Pro 2 on 9/3/16.
//  Copyright © 2016 Deep Gami. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TypesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTypeName;
@property (weak, nonatomic) IBOutlet UILabel *lblTypePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblLine;

@end
